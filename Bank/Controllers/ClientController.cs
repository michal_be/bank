﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bank.Models;
using Bank.Services;

namespace Bank.Controllers
{
    [Authorize]
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        public ActionResult ClientsPage()
        {
            return View();
        }

        public ActionResult ClientsList(string searchString, string searchBy)
        {
            var clientsList = _clientService.GetClientsViewModel(searchString, searchBy);
            return PartialView(clientsList);
        }


        [HttpGet]
        public ActionResult AddClient()
        {
            var addClientViewModel = _clientService.AddClientFormViewModel();
            return View(addClientViewModel);
        }

        [HttpPost]
        public ActionResult AddClient(ClientAndContactFormViewModel addClientViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(addClientViewModel);
            }
            _clientService.AddClient(addClientViewModel);

            return RedirectToAction("ClientsPage", "Client");
        }

        [HttpGet]
        public ActionResult EditContact(int clientId)
        {
            var contactViewModel = _clientService.GetContactByClientId(clientId);
            return View(contactViewModel);
        }

        [HttpPost]
        public ActionResult EditContact(ContactFormViewModel contactViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(contactViewModel);
            }
            _clientService.UpdateContact(contactViewModel);

            return View("ClientsPage");

        }

    }
}