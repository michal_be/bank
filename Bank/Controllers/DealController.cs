﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bank.Models;
using Bank.Services;

namespace Bank.Controllers
{
    [Authorize]
    public class DealController : Controller
    {
        private readonly IProductService _productService;
        private readonly IDealService _dealService;

        public DealController(IProductService productService, IDealService dealService)
        {
            _productService = productService;
            _dealService = dealService;

        }

        public ActionResult Statistics()
        {
            var chartData = _dealService.GetChartViewModel();
            return View(chartData);
        }



        public ActionResult ShowOffer()
        {
            return View();
        }

        // TODO : dealsOFClientList is entites list !! should be viewmodel list
        public ActionResult DealsOfClient(int clientId)
        {
            var dealsOfClientList = _dealService.GetActiveDealsOfClient(clientId);
            return View(dealsOfClientList);

        }

        public ActionResult SelectNewDeal(int clientId)
        {
            return View(clientId);
        }

        [HttpGet]
        public ActionResult NewCredit(int clientId)
        {
            var viewModel = _productService.GetCreditViewModel();
            viewModel.ClientId = clientId;
            return View(viewModel);

        }

        [HttpPost]
        public ActionResult NewCredit(CreditViewModel creditViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(creditViewModel);
            }

            _productService.AddCredit(creditViewModel);

            return RedirectToAction("ClientsPage", "Client");

        }

        [HttpGet]
        public ActionResult NewAccount(int clientId)
        {
            var viewModel = _productService.GetBankAccountViewModel();
            viewModel.ClientId = clientId;
            return View(viewModel);

        }

        [HttpPost]
        public ActionResult NewAccount(BankAccountViewModel bankAccountViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(bankAccountViewModel);
            }

            _productService.AddAccount(bankAccountViewModel);

            return RedirectToAction("ClientsPage", "Client");
        }

        [HttpGet]
        public ActionResult NewDeposit(int clientId)
        {
            var viewModel = _productService.GetDepositViewModel();
            viewModel.ClientId = clientId;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult NewDeposit(DepositViewModel depositViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(depositViewModel);
            }

            _productService.AddDeposit(depositViewModel);

            return RedirectToAction("ClientsPage", "Client");
        }

        [HttpGet]
        public ActionResult EditCredit(int productId)
        {
            var creditViewModel = _productService.GetCreditViewModelByProductId(productId);
            return View(creditViewModel);
        }

        [HttpPost]
        public ActionResult EditCredit(CreditViewModel creditViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(creditViewModel);
            }

            _productService.UpdateCredit(creditViewModel);

            return RedirectToAction("DealsOfClient", new { clientId = creditViewModel.ClientId });
        }

        [HttpGet]
        public ActionResult EditAccount(int productId)
        {
            var accountViewModel = _productService.GetBankAccountViewModelByProductId(productId);
            return View(accountViewModel);
        }

        [HttpPost]
        public ActionResult EditAccount(BankAccountViewModel bankAccountViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(bankAccountViewModel);
            }

            _productService.UpdateAccount(bankAccountViewModel);

            return RedirectToAction("DealsOfClient", new { clientId = bankAccountViewModel.ClientId });
        }

        [HttpGet]
        public ActionResult EditDeposit(int productId)
        {
            var depositViewModel = _productService.GetDepositViewModelByProductId(productId);
            return View(depositViewModel);
        }

        [HttpPost]
        public ActionResult EditDeposit(DepositViewModel depositViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(depositViewModel);
            }

            _productService.UpdateDeposit(depositViewModel);

            return RedirectToAction("DealsOfClient", new { clientId = depositViewModel.ClientId });
        }



    }
}