﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bank.Entities
{
    public abstract class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string Name { get; set; }
        public DateTime DateStart { get; set; } 
        public DateTime DateEnd { get; set; } 
        public string Currency { get; set; }
        public bool Active { get; set; }
        public string AdditionalInfo { get; set; }    
        public virtual ICollection<Deal> Deals { get; set; }
        public double RateOfInterest { get; set; }
    }
}