﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Web;

namespace Bank.Entities
{
    public class Account: Product
    {
        public double MonthlyAccountPayment { get; set; }
        public double MonthlyCardPayment { get; set; }
        public int PaymentCostFromOtherBank { get; set; }
        public bool AvailableAbroad { get; set; }      
        public bool InternetAccount { get; set; }

    }
}