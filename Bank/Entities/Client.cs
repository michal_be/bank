﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace Bank.Entities
{
    public class Client
    {
        [Key]
        public int ClientId { get; set; }
        public string FullName { get; set; }
        public string Pesel { get; set; }
        public string IdCardNumber { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual ICollection<Deal> Deals { get; set; }
    }
}