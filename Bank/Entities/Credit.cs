﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bank.Entities
{
    public class Credit : Product
    {
        public int Value { get; set; }
   
        public bool AdditionalInsurance { get; set; }

        public int AmountOfInstalment { get; set; }

        public int SubscribedSum { get; set; }

    }
}