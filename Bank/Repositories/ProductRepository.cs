﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Bank.Entities;
using Bank.Models;

namespace Bank.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _context;

        public ProductRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public int AddCreditAndReturnId(Credit credit)
        {
            _context.Products.Add(credit);
            _context.SaveChanges();
            var productd = credit.ProductId;
            return productd;
        }

        public int AddAccountAndReturnId(Account account)
        {
            _context.Products.Add(account);
            _context.SaveChanges();
            var productId = account.ProductId;
            return productId;
        }

        public int AddDepositAndReturnId(Deposit deposit)
        {
            _context.Products.Add(deposit);
            _context.SaveChanges();
            var productId = deposit.ProductId;
            return productId;
        }

        public void AddDeal(Deal deal)
        {
            _context.Deals.Add(deal);
            _context.SaveChanges();
        }

        public Credit GetCreditByProductId(int productId)
        {
            var credit = _context.Products.OfType<Credit>().SingleOrDefault(x => x.ProductId == productId);
            return credit;
        }

        public void UpdateCredit(Credit credit)
        {
            var original = _context.Products.OfType<Credit>().SingleOrDefault(x => x.ProductId == credit.ProductId);

            if (original != null)
            {
                _context.Entry(original).CurrentValues.SetValues(credit);
                _context.SaveChanges();
            }
        }

        public Account GetAccountByProductId(int productId)
        {
            var account = _context.Products.OfType<Account>().SingleOrDefault(x => x.ProductId == productId);
            return account;
        }

        public void UpdateAccount(Account account)
        {
            var original = _context.Products.OfType<Account>().SingleOrDefault(x => x.ProductId == account.ProductId);

            if (original != null)
            {
                _context.Entry(original).CurrentValues.SetValues(account);
                _context.SaveChanges();
            }
        }

        public Deposit GetDepositByProductId(int productId)
        {
            var deposit = _context.Products.OfType<Deposit>().SingleOrDefault(x => x.ProductId == productId);
            return deposit;
        }

        public void UpdateDeposit(Deposit deposit)
        {
            var original = _context.Products.OfType<Deposit>().SingleOrDefault(x => x.ProductId == deposit.ProductId);

            if (original != null)
            {
                _context.Entry(original).CurrentValues.SetValues(deposit);
                _context.SaveChanges();
            }

        }

        public int GetAmountOfDeposits()
        {
            var amountOfDeposits = _context.Products.OfType<Deposit>().Count();
            return amountOfDeposits;
        }

        public int GetAmountOfAccounts()
        {
            var amountOfAccounts = _context.Products.OfType<Account>().Count();
            return amountOfAccounts;
        }

        public int GetAmountOfCredits()
        {
            var amountOfCredits = _context.Products.OfType<Credit>().Count();
            return amountOfCredits;
        }
    }
}