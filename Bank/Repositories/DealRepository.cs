﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bank.Entities;
using Bank.Models;

namespace Bank.Repositories
{
    public class DealRepository : IDealRepository
    {
        private readonly ApplicationDbContext _context;
        public DealRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Deal> GetActiveDealsOfClient(int clientId)
        {
            var dealsOfClientList = _context.Deals.Where(x => x.ClientId == clientId && x.Product.Active).ToList();
            return dealsOfClientList;
        }

        public bool CheckIfClientHaveAnyActiveDeals(int clientId)
        {
            bool haveAnyActive = _context.Deals.Any(x => x.ClientId == clientId && x.Product.Active == true);
            return haveAnyActive;
        }

        public int GetClientIdByProductId(int productId)
        {
            var clientId = _context.Deals.Single(x => x.ProductId == productId).ClientId;
            return clientId;
        }

        public int GetAmountOfDeals()
        {
            var amountOfDeals = _context.Deals.Count();
            return amountOfDeals;
        }

    }
}