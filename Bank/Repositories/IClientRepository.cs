﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Entities;

namespace Bank.Repositories
{
    public interface IClientRepository
    {
        void AddClient(Client client);
        Client GetClient(int clientId);
        void SaveUpdatedClient(Client client);
        List<Client> GetClientsList();
        Client GetClientByPesel(string pesel);
        string GetClientFullNameById(int clientId);
        int GetAmountOfClients();
    }
}
