﻿using System.Collections.Generic;
using Bank.Entities;

namespace Bank.Repositories
{
    public interface IDealRepository
    {
        List<Deal> GetActiveDealsOfClient(int clientId);
        bool CheckIfClientHaveAnyActiveDeals(int clientId);
        int GetClientIdByProductId(int productId);
        int GetAmountOfDeals();
   
    }
}