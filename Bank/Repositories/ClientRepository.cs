﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using Bank.Entities;
using Bank.Models;

namespace Bank.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private readonly ApplicationDbContext _context;
        public ClientRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddClient(Client client)
        {
            _context.Clients.Add(client);
            _context.SaveChanges();
        }

        public Client GetClientByPesel(string pesel)
        {
            var client = _context.Clients.SingleOrDefault(x => x.Pesel == pesel);
            return client;
        }

        public Client GetClient(int clientId)
        {
            var client = _context.Clients.SingleOrDefault(x => x.ClientId == clientId);
            return client;
        }

        public void SaveUpdatedClient(Client client)
        {
            _context.Entry(client).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public List<Client> GetClientsList()
        {
            var clientsList = _context.Clients.ToList();
            return clientsList;
        }

        public string GetClientFullNameById(int clientId)
        {
            var fullName = _context.Clients.Single(x => x.ClientId == clientId).FullName;
            return fullName;
        }

        public int GetAmountOfClients()
        {
            var amountOfClients = _context.Clients.Count();
            return amountOfClients;
        }
    }
}