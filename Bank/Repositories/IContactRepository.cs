﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Entities;

namespace Bank.Repositories
{
    public interface IContactRepository
    {
        void AddContact(Contact contact);
        Contact GetContactByClientId(int clientId);
        void UpdateContact(Contact contact);
    }
}
