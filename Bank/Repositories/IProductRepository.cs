﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Entities;

namespace Bank.Repositories
{
    public interface IProductRepository
    {
        int AddCreditAndReturnId(Credit credit);
        int AddAccountAndReturnId(Account account);
        int AddDepositAndReturnId(Deposit deposit);
        void AddDeal(Deal deal);
        Credit GetCreditByProductId(int productId);
        void UpdateCredit(Credit credit);
        Account GetAccountByProductId(int productId);
        void UpdateAccount(Account account);
        Deposit GetDepositByProductId(int productId);
        void UpdateDeposit(Deposit deposit);
        int GetAmountOfDeposits();
        int GetAmountOfAccounts();
        int GetAmountOfCredits();
    }
}
