﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Bank.Entities;
using Bank.Models;

namespace Bank.Repositories
{
    public class ContactRepository : IContactRepository
    {
        private readonly ApplicationDbContext _context;

        public ContactRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddContact(Contact contact)
        {
            contact = _context.Contacts.Add(contact);
            var dbEntry = _context.Clients.Single(x => x.ClientId == contact.OwnerId);
            dbEntry.Contact = contact;
            _context.SaveChanges();

        }

        public Contact GetContactByClientId(int clientId)
        {
            var contact = _context.Contacts.SingleOrDefault(x => x.OwnerId == clientId);
            return contact;
        }

        public void UpdateContact(Contact contact)
        {
            var original = _context.Contacts.SingleOrDefault(x => x.OwnerId == contact.OwnerId);

            if (original != null)
            {
                original.Adress = contact.Adress;
                original.Email = contact.Email;
                original.PhoneNumber = contact.PhoneNumber;
            }
            
            _context.SaveChanges();
        }

    }
}