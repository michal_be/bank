﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bank.Controllers;
using Bank.Entities;
using Bank.Infrastructure;
using Bank.Models;
using Bank.Repositories;

namespace Bank.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IAuthInfrastructure _authInfrastructure;
        private readonly IDealRepository _dealRepository;
        private readonly IMappingInfrastructure _mappingInfrastructure;

        public ProductService(IProductRepository productRepository, IAuthInfrastructure authInfrastructure, IDealRepository dealRepository, IMappingInfrastructure mappingInfrastructure)
        {
            _productRepository = productRepository;
            _authInfrastructure = authInfrastructure;
            _dealRepository = dealRepository;
            _mappingInfrastructure = mappingInfrastructure;
        }

        public CreditViewModel GetCreditViewModel()
        {
            var creditViewModel = new CreditViewModel();
            return creditViewModel;
        }

        public BankAccountViewModel GetBankAccountViewModel()
        {
            var bankAccountViewModel = new BankAccountViewModel();
            return bankAccountViewModel;
        }

        public DepositViewModel GetDepositViewModel()
        {
            var depositViewModel = new DepositViewModel();
            return depositViewModel;
        }

        public void AddCredit(CreditViewModel creditViewModel)
        {

            var credit = _mappingInfrastructure.MapCreditViewModelToCredit(creditViewModel);
            credit.Active = true;         
            var productId = _productRepository.AddCreditAndReturnId(credit);
            AddDeal(creditViewModel.ClientId, productId);
        }

        public void AddDeal(int clientId, int productId)
        {
            var deal = new Deal()
            {
                ClientId = clientId,
                ProductId = productId,
                StartDate = DateTime.Today,
                EndDate = DateTime.MaxValue,
                AdvisorId = _authInfrastructure.GetCurrentUserId()
            };

            _productRepository.AddDeal(deal);
        }

        public void AddAccount(BankAccountViewModel bankAccountViewModel)
        {
            var account = _mappingInfrastructure.MapBankAccountViewModelToAccount(bankAccountViewModel);
            account.Active = true;          
            var productId = _productRepository.AddAccountAndReturnId(account);
            AddDeal(bankAccountViewModel.ClientId, productId);
        }

        public void AddDeposit(DepositViewModel depositViewModel)
        {
            var deposit = _mappingInfrastructure.MapDepositViewModelToDeposit(depositViewModel);
            deposit.Active = true;
            var productId = _productRepository.AddDepositAndReturnId(deposit);
            AddDeal(depositViewModel.ClientId, productId);
        }

        public CreditViewModel GetCreditViewModelByProductId(int productId)
        {
            var credit = _productRepository.GetCreditByProductId(productId);
            var clientId = _dealRepository.GetClientIdByProductId(productId);
            var creditViewModel = _mappingInfrastructure.MapCreditToCreditViewModel(credit);
            creditViewModel.Active = true;
            creditViewModel.ClientId = clientId;         
            return creditViewModel;
        }

        public void UpdateCredit(CreditViewModel creditViewModel)
        {
            var credit = _mappingInfrastructure.MapCreditViewModelToCredit(creditViewModel);
            _productRepository.UpdateCredit(credit);
        }

        public BankAccountViewModel GetBankAccountViewModelByProductId(int productId)
        {
            var account = _productRepository.GetAccountByProductId(productId);
            var clientId = _dealRepository.GetClientIdByProductId(productId);
            var bankAccountViewModel = _mappingInfrastructure.MapAccountToBankAccountViewModel(account);
            bankAccountViewModel.ClientId = clientId;

            return bankAccountViewModel;
        }

        public void UpdateAccount(BankAccountViewModel bankAccountViewModel)
        {
            var account = _mappingInfrastructure.MapBankAccountViewModelToAccount(bankAccountViewModel);
            _productRepository.UpdateAccount(account);
        }

        public DepositViewModel GetDepositViewModelByProductId(int productId)
        {
            var deposit = _productRepository.GetDepositByProductId(productId);
            var clientId = _dealRepository.GetClientIdByProductId(productId);
            var depositViewModel = _mappingInfrastructure.MapDepositToDepositViewModel(deposit);
            depositViewModel.ClientId = clientId;
      
            return depositViewModel;
        }

        public void UpdateDeposit(DepositViewModel depositViewModel)
        {
            var deposit = _mappingInfrastructure.MapDepositViewModelToDeposit(depositViewModel);
            _productRepository.UpdateDeposit(deposit);
        }
    }
}
