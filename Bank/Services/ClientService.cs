﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bank.Entities;
using Bank.Infrastructure;
using Bank.Models;
using Bank.Repositories;

namespace Bank.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IContactRepository _contactRepository;
        private readonly IDealRepository _dealRepository;
        private readonly IMappingInfrastructure _mappingInfrastructure;

        public ClientService(IClientRepository clientRepository, IContactRepository contactRepository, IDealRepository dealRepository, IMappingInfrastructure mappingInfrastructure)
        {
            _clientRepository = clientRepository;
            _contactRepository = contactRepository;
            _dealRepository = dealRepository;
            _mappingInfrastructure = mappingInfrastructure;
        }

        public void AddClient(ClientAndContactFormViewModel addClientViewModel)
        {

            var client = _mappingInfrastructure.MapClientAndContactFormViewModelToClient(addClientViewModel);
             _clientRepository.AddClient(client);

            var clientData = _clientRepository.GetClientByPesel(client.Pesel);
            var contact = _mappingInfrastructure.MapClientAndContactFormViewModelToContact(addClientViewModel);
            contact.OwnerId = clientData.ClientId;            
            _contactRepository.AddContact(contact);
        }

        public ClientFormViewModel GetClientEditViewModel(int clientId)
        {
            var client = _clientRepository.GetClient(clientId);
            var viewModel = new ClientFormViewModel(client.ClientId, client.FullName, client.Pesel, client.IdCardNumber);

            return viewModel;
        }

        public ClientAndContactFormViewModel AddClientFormViewModel()
        {
            var addClientViewModel = new ClientAndContactFormViewModel();
            return addClientViewModel;
        }

        public List<ClientFormViewModel> GetClientsViewModel(string searchString, string searchBy)
        {
      
           var clientsList = _clientRepository.GetClientsList();

           if (searchBy == "FullName")
            {
                if (!String.IsNullOrEmpty(searchString))
                {
                    clientsList = clientsList.Where(x => x.FullName.Contains(searchString)).ToList();
                }
            }
           else
            {
                if (!String.IsNullOrEmpty(searchString))
                {
                    clientsList = clientsList.Where(x => x.Pesel.Contains(searchString)).ToList();
                }
            }

            List<ClientFormViewModel> clientsListViewModel =
                _mappingInfrastructure.MapClientListToClientFormViewModelList(clientsList);

            foreach (var client in clientsListViewModel)
            {
                client.HaveAnyActiveDeal = _dealRepository.CheckIfClientHaveAnyActiveDeals(client.ClientId);
            }
       
            return clientsListViewModel;
        }

        public ContactFormViewModel GetContactByClientId(int clientId)
        {
            var contact = _contactRepository.GetContactByClientId(clientId);
            var clientName = _clientRepository.GetClientFullNameById(clientId); 
            var contactViewModel = _mappingInfrastructure.MapContactToContactFormViewModel(contact);
            contactViewModel.ClientId = clientId;
            contactViewModel.FullName = clientName;

            return contactViewModel;
        }

        public void UpdateContact(ContactFormViewModel contactViewModel)
        {

            var contact = _mappingInfrastructure.MapContactFormViewModelToContact(contactViewModel);
            contact.OwnerId = contactViewModel.ClientId;
            _contactRepository.UpdateContact(contact);
        }
         
    }
}