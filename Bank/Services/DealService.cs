﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bank.Entities;
using Bank.Models;
using Bank.Repositories;

namespace Bank.Services
{
    public class DealService : IDealService
    {
        private readonly IDealRepository _dealRepository;
        private readonly IProductRepository _productRepository;
        private readonly IClientRepository _clientRepository;

        public DealService(IDealRepository dealRepository, IProductRepository productRepository, IClientRepository clientRepository)
        {
            _dealRepository = dealRepository;
            _productRepository = productRepository;
            _clientRepository = clientRepository;
        }

        public List<Deal> GetActiveDealsOfClient(int clientId)
        {
            var dealsOfClientList = _dealRepository.GetActiveDealsOfClient(clientId);                              
            return dealsOfClientList;
        }

        public ChartViewModel GetChartViewModel()
        {
            var chartData = new ChartViewModel()
            {
                AmountOfDeals = _dealRepository.GetAmountOfDeals(),
                AmountOfDeposits = _productRepository.GetAmountOfDeposits(),
                AmountOfAccounts = _productRepository.GetAmountOfAccounts(),
                AmountOfClients = _clientRepository.GetAmountOfClients(),
                AmountOfCredits = _productRepository.GetAmountOfCredits()
            };

            return chartData;
        }
    }
}