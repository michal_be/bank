﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Entities;
using Bank.Models;

namespace Bank.Services
{
    public interface IClientService
    {
        void AddClient(ClientAndContactFormViewModel addClientViewModel);
        ClientFormViewModel GetClientEditViewModel(int clientId);
        ClientAndContactFormViewModel AddClientFormViewModel();
        List<ClientFormViewModel> GetClientsViewModel(string searchString, string searchBy);
        ContactFormViewModel GetContactByClientId(int clientId);
        void UpdateContact(ContactFormViewModel contactViewModel);
    }
}
