﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bank.Models;

namespace Bank.Services
{
    public interface IProductService
    {
        CreditViewModel GetCreditViewModel();

        void AddCredit(CreditViewModel creditViewModel);

        BankAccountViewModel GetBankAccountViewModel();

        void AddAccount(BankAccountViewModel bankAccountViewModel);
        DepositViewModel GetDepositViewModel();

        void AddDeposit(DepositViewModel depositViewModel);
  
        CreditViewModel GetCreditViewModelByProductId(int productId);
        void UpdateCredit(CreditViewModel creditViewModel);

        BankAccountViewModel GetBankAccountViewModelByProductId(int productId);
        void UpdateAccount(BankAccountViewModel bankAccountViewModel);
        DepositViewModel GetDepositViewModelByProductId(int productId);

        void UpdateDeposit(DepositViewModel depositViewModel);
    }
}