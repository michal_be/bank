﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank.Entities;
using Bank.Models;

namespace Bank.Services
{
    public interface IDealService
    {
        List<Deal> GetActiveDealsOfClient(int clientId);
        ChartViewModel GetChartViewModel();
    }
}
