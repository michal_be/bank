using Bank.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Bank.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Bank.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Bank.Models.ApplicationDbContext";
        }

        protected override void Seed(Bank.Models.ApplicationDbContext context)
        {
            if (!context.Users.Any(u => u.UserName == "admin@bank.com"))
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var user = new ApplicationUser { UserName = "admin@bank.com" };

                userManager.Create(user, "Admin123!");
                roleManager.Create(new IdentityRole { Name = "admin" });

                userManager.AddToRole(user.Id, "admin");
            }

        }
    }
}
