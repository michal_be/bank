﻿function animateCreditForm() {

    $("#field1").css({ visibility: "visible", opacity: 0.0 }).animate({ opacity: 1.0 }, 500);
    $("#field2").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(500).animate({ opacity: 1.0 }, 500);
    $("#field3").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(1000).animate({ opacity: 1.0 }, 500);
    $("#field4").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(1500).animate({ opacity: 1.0 }, 500);
    $("#field5").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(2000).animate({ opacity: 1.0 }, 500);
    $("#field6").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(2500).animate({ opacity: 1.0 }, 500);
    $("#field7").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(3000).animate({ opacity: 1.0 }, 500);
    $("#field8").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(3500).animate({ opacity: 1.0 }, 500);
    $("#field9").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(4000).animate({ opacity: 1.0 }, 500);
    $("#field10").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(4500).animate({ opacity: 1.0 }, 500);
    $("#field11").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(5000).animate({ opacity: 1.0 }, 500);

    $('option[value="2,50"]').hide();
    $('option[value="14,90"]').hide();
    $('option[value="7,15"]').hide();

}


function setRateOptionsByCreditType(type) {
 
    var credit = type;
    if (credit === "Kredyt gotówkowy") {
        $('#rate').prop('selectedIndex', 1);
    }
    else if (credit === "Kredyt hipoteczny") {
        $('#rate').prop('selectedIndex', 2);
    }
    else if (credit === "Kredyt samochodowy") {
        $('#rate').prop('selectedIndex', 3);
    }
}