﻿function setRateOptionsByDepositType(type) {

    if (type === "Lokata standardowa") {
        $('#rate').prop('selectedIndex', 0);
        $('option[value="0,60"]').hide();
        $('option[value="1,70"]').show();
        $('option[value="2,00"]').show();
        $('option[value="2,20"]').show();
        $('option[value="2,70"]').show();
    }
    else if (type === "Rachunek oszczędnościowy") {
        $('#rate').prop('selectedIndex', 0);
        $('option[value="0,60"]').show();
        $('option[value="1,70"]').hide();
        $('option[value="2,00"]').hide();
        $('option[value="2,20"]').hide();
        $('option[value="2,70"]').hide();
    }
}