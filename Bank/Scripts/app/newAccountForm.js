﻿
function animateAccountForm() {

    $("#field1").css({ visibility: "visible", opacity: 0.0 }).animate({ opacity: 1.0 }, 500);
    $("#field2").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(500).animate({ opacity: 1.0 }, 500);
    $("#field3").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(1000).animate({ opacity: 1.0 }, 500);
    $("#field4").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(1500).animate({ opacity: 1.0 }, 500);
    $("#field5").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(2000).animate({ opacity: 1.0 }, 500);
    $("#field6").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(2500).animate({ opacity: 1.0 }, 500);
    $("#field7").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(3000).animate({ opacity: 1.0 }, 500);
    $("#field8").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(3500).animate({ opacity: 1.0 }, 500);
    $("#field9").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(4000).animate({ opacity: 1.0 }, 500);
    $("#field10").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(4500).animate({ opacity: 1.0 }, 500);
    $("#field11").css({ visibility: "visible", opacity: 0.0 }).stop(true, true).delay(5000).animate({ opacity: 1.0 }, 500);

}


function setCurrencyOptionsByAccountType(type) {

    if (type === "Student") {
        $('#currency').prop('selectedIndex', 0);
        $('option[value="EUR"]').hide();
        $('option[value="USD"]').hide();
        $('option[value="GBP"]').hide();
        $('option[value="CHF"]').hide();
    } else if (type === "Konto plus") {
        $('#currency').prop('selectedIndex', 0);
        $('option[value="EUR"]').show();
        $('option[value="USD"]').show();
        $('option[value="GBP"]').hide();
        $('option[value="CHF"]').hide();
    } else if (type === "Konto premium") {
        $('#currency').prop('selectedIndex', 0);
        $('option[value="EUR"]').show();
        $('option[value="USD"]').show();
        $('option[value="GBP"]').show();
        $('option[value="CHF"]').show();
    }
}