﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using AutoMapper;
using Bank.Entities;
using Bank.Models;

namespace Bank.App_Start
{
    public static class AutoMapperConfiguration
    {
        public static IMapper ConfigureMapper()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ContactFormViewModel, Contact>();
                cfg.CreateMap<Contact, ContactFormViewModel>();
                cfg.CreateMap<ClientAndContactFormViewModel, Contact>();
                cfg.CreateMap<ClientAndContactFormViewModel, Client>();
                cfg.CreateMap<Client, ClientFormViewModel>();
                cfg.CreateMap<CreditViewModel, Credit>();
                cfg.CreateMap<BankAccountViewModel, Account>();
                cfg.CreateMap<Account, BankAccountViewModel>();
                cfg.CreateMap<DepositViewModel, Deposit>();
                cfg.CreateMap<Credit, CreditViewModel>();
                cfg.CreateMap<Deposit, DepositViewModel>();


            });
            var mapper = configuration.CreateMapper();
            return mapper;
        }
    }

}