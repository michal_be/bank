﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace Bank.Infrastructure
{
    public class AuthInfrastructure : IAuthInfrastructure
    {
        public string GetCurrentUserId()
        {
            var context = HttpContext.Current;
            var owinContext = context.GetOwinContext();
            var authentication = owinContext.Authentication;
            var user = authentication.User;
            var userId = user.Identity.GetUserId();
            return userId;
        }
    }
}