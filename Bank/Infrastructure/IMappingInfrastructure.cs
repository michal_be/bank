﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bank.Entities;
using Bank.Models;

namespace Bank.Infrastructure
{
    public interface IMappingInfrastructure
    {

        Contact MapContactFormViewModelToContact(ContactFormViewModel contactFormViewModel);
        ContactFormViewModel MapContactToContactFormViewModel(Contact contact);
        Contact MapClientAndContactFormViewModelToContact(ClientAndContactFormViewModel addClientViewModel);
        Client MapClientAndContactFormViewModelToClient(ClientAndContactFormViewModel addClientViewModel);
        List<ClientFormViewModel> MapClientListToClientFormViewModelList(List<Client> clients);
        Credit MapCreditViewModelToCredit(CreditViewModel creditViewModel);
        Account MapBankAccountViewModelToAccount(BankAccountViewModel bankAccountViewModel);
        Deposit MapDepositViewModelToDeposit(DepositViewModel depositViewModel);
        CreditViewModel MapCreditToCreditViewModel(Credit credit);
        BankAccountViewModel MapAccountToBankAccountViewModel(Account account);
        DepositViewModel MapDepositToDepositViewModel(Deposit deposit);
    }
}