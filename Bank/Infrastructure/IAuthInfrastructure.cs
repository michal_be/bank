﻿namespace Bank.Infrastructure
{
    public interface IAuthInfrastructure
    {
        string GetCurrentUserId();
    }
}