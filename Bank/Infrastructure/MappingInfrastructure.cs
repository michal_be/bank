﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Bank.Entities;
using Bank.Models;

namespace Bank.Infrastructure
{
    public class MappingInfrastructure : IMappingInfrastructure
    {
        private readonly IMapper _mapper;

        public MappingInfrastructure(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Contact MapContactFormViewModelToContact(ContactFormViewModel contactFormViewModel)
        {
            var contact = _mapper.Map<ContactFormViewModel, Contact>(contactFormViewModel);
            return contact;
        }
        public ContactFormViewModel MapContactToContactFormViewModel(Contact contact)
        {
            var contactFormViewModel = _mapper.Map<Contact, ContactFormViewModel>(contact);
            return contactFormViewModel;
        }

        public Contact MapClientAndContactFormViewModelToContact(ClientAndContactFormViewModel addClientViewModel)
        {
            var contact = _mapper.Map<ClientAndContactFormViewModel, Contact>(addClientViewModel);
            return contact;
        }

        public Client MapClientAndContactFormViewModelToClient(ClientAndContactFormViewModel addClientViewModel)
        {
            var client = _mapper.Map<ClientAndContactFormViewModel, Client>(addClientViewModel);
            return client;
        }

        public List<ClientFormViewModel> MapClientListToClientFormViewModelList(List<Client> clients)
        {
            
            var clientFormViewModelList = _mapper.Map<List<Client>, List<ClientFormViewModel>>(clients);
            return clientFormViewModelList;

        }

        public Credit MapCreditViewModelToCredit(CreditViewModel creditViewModel)
        {
            var credit = _mapper.Map<CreditViewModel, Credit>(creditViewModel);
            return credit;
        }

        public Account MapBankAccountViewModelToAccount(BankAccountViewModel bankAccountViewModel)
        {
            var account = _mapper.Map<BankAccountViewModel, Account>(bankAccountViewModel);
            return account;
        }

        public Deposit MapDepositViewModelToDeposit(DepositViewModel depositViewModel)
        {
            var deposit = _mapper.Map<DepositViewModel, Deposit>(depositViewModel);
            return deposit;
        }

        public CreditViewModel MapCreditToCreditViewModel(Credit credit)
        {
            var creditViewModel = _mapper.Map<Credit, CreditViewModel>(credit);
            return creditViewModel;
        }

        public BankAccountViewModel MapAccountToBankAccountViewModel(Account account)
        {
            var bankAccountViewModel = _mapper.Map<Account, BankAccountViewModel>(account);
            return bankAccountViewModel;
        }

        public DepositViewModel MapDepositToDepositViewModel(Deposit deposit)
        {
            var depositViewModel = _mapper.Map<Deposit, DepositViewModel>(deposit);
            return depositViewModel;
        }
    }
}