﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Bank.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Bank.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
 
        public virtual IDbSet<Client> Clients { get; set; } 
        public virtual IDbSet<Contact> Contacts { get; set; }
        public virtual IDbSet<Deal> Deals { get; set; } 
        public virtual IDbSet<Product> Products { get; set; } 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
             .HasOptional(u => u.Contact)
             .WithRequired(u => u.Owner);

            modelBuilder.Entity<Deal>()
                .HasRequired<Client>(s => s.Client)
                .WithMany(s => s.Deals);

            modelBuilder.Entity<Deal>()
                .HasRequired<Product>(s => s.Product)
                .WithMany(s => s.Deals);

            base.OnModelCreating(modelBuilder);
        }



        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}