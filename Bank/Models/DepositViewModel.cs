﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bank.Models
{
    public class DepositViewModel
    {
        public int ProductId { get; set; }

        [DisplayName("Nazwa")]
        [Required]
        public string Name { get; set; }
        public SelectList Names { get; set; }

        [DisplayName("Data aktywacji")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime DateStart { get; set; }

        [DisplayName("Data zakończenia")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime DateEnd { get; set; }

        [DisplayName("Waluta")]
        [Required]
        public string Currency { get; set; }

        public SelectList Currencies { get; set; }

        [DisplayName("Oprocentowanie lokaty")]
        [Required]
        public double RateOfInterest { get; set; }

        public SelectList RateOfInterests { get; set; }

        [DisplayName("Wpłacona kwota")]
        [Required]
        public double SubscribedSum { get; set; }

        [DisplayName("Dodatkowe informacje")]
        public string AdditionalInfo { get; set; }

        [DisplayName("Aktywny")]
        public bool Active { get; set; }
        public int ClientId { get; set; }

        public DepositViewModel()
        {

            Names = new SelectList(new[]
                     {
                           new SelectListItem { Value = "Lokata standardowa", Text = "Lokata standardowa" },
                           new SelectListItem { Value = "Rachunek oszczędnościowy", Text = "Rachunek oszczędnościowy" },
                   
                       }, "Value", "Text");

            Currencies = new SelectList(new[]
                    {
                           new SelectListItem {Text = "PLN", Value = "PLN"},
                           new SelectListItem {Text = "EUR", Value = "EUR"},
                           new SelectListItem {Text = "USD", Value = "USD"},
                           new SelectListItem {Text = "GBP", Value = "GBP"},
                           new SelectListItem {Text = "CHF", Value = "CHF"}
                       }, "Text", "Value");


            RateOfInterests = new SelectList(new[]
                        {
                           new SelectListItem {Value = "0,60", Text = "0.6 %",},
                           new SelectListItem {Value = "1,70", Text = "1.70 %" },
                           new SelectListItem {Value = "2,00", Text = "2 %"},
                           new SelectListItem {Value = "2,20", Text = "2.20 %"},
                           new SelectListItem {Value = "2,70", Text = "2.70 %"},
                       }, "Value", "Text");

        }

    }
}