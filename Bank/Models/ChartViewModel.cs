﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class ChartViewModel
    {
        public int AmountOfDeals { get; set; }
        public int AmountOfClients { get; set; }
        public int AmountOfCredits { get; set; }
        public int AmountOfAccounts { get; set; }
        public int AmountOfDeposits { get; set; }

    }
}