﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class ContactFormViewModel
    {
        public int Id { get; set; }

        [DisplayName("Numer telefonu")]
        public string PhoneNumber { get; set; }

        [DisplayName("Adres email")]
        public string Email { get; set; }

        [DisplayName("Adres zamieszkania")]
        public string Adress { get; set; }

        public string FullName { get; set; }

        public int ClientId { get; set; }

    }
}