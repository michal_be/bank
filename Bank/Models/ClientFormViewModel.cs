﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class ClientFormViewModel
    {
        public int ClientId { get; set; }

        [DisplayName("Imię i nazwisko")]
        public string FullName { get; set; }

        [DisplayName("PESEL")]
        public string Pesel { get; set; }

        [DisplayName("Numer dowodu osobistego")]
        public string IdCardNumber { get; set; }

        public bool HaveAnyActiveDeal { get; set; }

        public ClientFormViewModel(int clientId, string fullName, string pesel, string idCardNumber)
        {
            ClientId = clientId;
            FullName = fullName;
            Pesel = pesel;
            IdCardNumber = idCardNumber;
        }

        public ClientFormViewModel()
        {
            HaveAnyActiveDeal = false;
        }
    }
}