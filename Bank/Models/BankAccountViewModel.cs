﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bank.Models
{
    public class BankAccountViewModel
    {
        public int ProductId { get; set; }

        [DisplayName("Nazwa")]
        [Required]
        public string Name { get; set; }
        public SelectList Names { get; set; }

        [DisplayName("Data aktywacji")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime DateStart { get; set; }

        [DisplayName("Data zakończenia")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime DateEnd { get; set; }

        [DisplayName("Waluta")]
        [Required]
        public string Currency { get; set; }

        public SelectList Currencies { get; set; }

        [DisplayName("Dodatkowe informacje")]
        public string AdditionalInfo { get; set; }

        [DisplayName("Oprocentowanie konta")]
        [Required]
        public double RateOfInterest { get; set; }
        public SelectList RateOfInterests { get; set; }

        [DisplayName("Miesięczny koszt prowadzenia konta")]
        [Required]
        public double MonthlyAccountPayment { get; set; }
        public SelectList MonthlyAccountPayments { get; set; }

        [DisplayName("Miesięczna opłata za kartę")]
        [Required]
        public double MonthlyCardPayment { get; set; }
        public SelectList MonthlyCardPayments { get; set; }

        [DisplayName("Koszt wypłaty z obcego bankomatu")]
        [Required]
        public int PaymentCostFromOtherBank { get; set; }
        public SelectList PaymentCostFromOtherBanks { get; set; }

        [DisplayName("Dostęp do środków za granicą")]
        public bool AvailableAbroad { get; set; }

        [DisplayName("Konto internetowe")]
        public bool InternetAccount { get; set; }

        [DisplayName("Aktywny")]
        public bool Active { get; set; }
        public int ClientId { get; set; }


        public BankAccountViewModel()
        {

            Names = new SelectList(new[]
                       {
                           new SelectListItem { Value = "Student", Text = "Student" },
                           new SelectListItem { Value = "Konto plus", Text = "Konto plus" },
                           new SelectListItem { Value = "Konto premium", Text = "Konto premium" }             
                       }, "Value", "Text");


            Currencies = new SelectList(new[]
                       {
                           new SelectListItem {Text = "PLN", Value = "PLN"},
                           new SelectListItem {Text = "EUR", Value = "EUR"},
                           new SelectListItem {Text = "USD", Value = "USD"},
                           new SelectListItem {Text = "GBP", Value = "GBP"},
                           new SelectListItem {Text = "CHF", Value = "CHF"}
                       }, "Text", "Value");


            RateOfInterests = new SelectList(new[]
                        {
                           new SelectListItem {Value = "0", Text = "0 %",},
                           new SelectListItem {Value = "2", Text = "2 %" },
                           new SelectListItem {Value = "5", Text = "5 %"},
                       }, "Value", "Text");


            MonthlyAccountPayments = new SelectList(new[]
            {
                new SelectListItem { Value = "0", Text = "Brak opłaty" },
                new SelectListItem { Value = "12,50", Text = "12,50 PLN" },
                new SelectListItem { Value = "25,00", Text = "25,00 PLN" },
            }, "Value", "Text");


            MonthlyCardPayments = new SelectList(new[]
            {
                new SelectListItem { Value = "0", Text = "Brak opłaty" },
                new SelectListItem { Value = "3,00", Text = "3,00 PLN" },
                new SelectListItem { Value = "5,00", Text = "5,00 PLN" },
            }, "Value", "Text");


            PaymentCostFromOtherBanks = new SelectList(new[]
            {
                new SelectListItem { Value = "0", Text = "Brak opłaty" },
                new SelectListItem { Value = "5", Text = "5,00 PLN" },
            }, "Value", "Text");

        }

    }
}