﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class ClientAndContactFormViewModel
    {
        public int Id { get; set; }

        [DisplayName("Imię i nazwisko")]
        public string FullName { get; set; }

        [DisplayName("PESEL")]
        public string Pesel { get; set; }

        [DisplayName("Numer dowodu osobistego")]
        public string IdCardNumber { get; set; }

        [DisplayName("Numer telefonu")]
        public string PhoneNumber { get; set; }

        [DisplayName("Adres email")]
        public string Email { get; set; }

        [DisplayName("Adres zamieszkania")]
        public string Adress { get; set; }

    }
}