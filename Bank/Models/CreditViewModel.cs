﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bank.Models
{
    public class CreditViewModel
    {
        public int ProductId { get; set; }

        [DisplayName("Nazwa")]
        [Required]
        public string Name { get; set; }

        public SelectList Names { get; set; }

        [DisplayName("Data aktywacji")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime DateStart { get; set; }

        [DisplayName("Data zakończenia")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime DateEnd { get; set; }

        [DisplayName("Waluta")]
        [Required]
        public string Currency { get; set; }
        public SelectList Currencies { get; set; }

        [DisplayName("Dodatkowe informacje")]
        public string AdditionalInfo { get; set; }

        [DisplayName("Wartość kredytu w wybranej walucie")]
        [Required]
        public int Value { get; set; }

        [DisplayName("Dodatkowe ubezpieczenie")]
        public bool AdditionalInsurance { get; set; }

        [DisplayName("Ilość rat")]
        [Required]
        public int AmountOfInstalment { get; set; }

        [DisplayName("Wkład własny")]
        [Required]
        public int SubscribedSum { get; set; }

        [DisplayName("Oprocentowanie w skali roku")]
        [Required]
        public double RateOfInterest { get; set; }

        public SelectList RateOfInterests { get; set; }

        [DisplayName("Aktywny")]
        public bool Active { get; set; }
        public int ClientId { get; set; }


        public CreditViewModel()
        {
            Names = new SelectList(new[]
                       {
                           new SelectListItem {Text = "Kredyt gotówkowy", Value = "Kredyt gotówkowy"},
                           new SelectListItem {Text = "Kredyt hipoteczny", Value = "Kredyt hipoteczny"},
                           new SelectListItem {Text = "Kredyt samochodowy", Value = "Kredyt samochodowy"},
                       }, "Text", "Value");


            Currencies = new SelectList(new[]
                       {
                           new SelectListItem {Text = "PLN", Value = "PLN"},
                           new SelectListItem {Text = "EUR", Value = "EUR"},
                           new SelectListItem {Text = "USD", Value = "USD"},
                           new SelectListItem {Text = "GBP", Value = "GBP"},
                           new SelectListItem {Text = "CHF", Value = "CHF"},
                       }, "Text", "Value");


            RateOfInterests = new SelectList(new[]
                        {
                           new SelectListItem {Value = "7,15", Text = "7.15 %",},
                           new SelectListItem {Value = "2,50", Text = "2.50 %" },
                           new SelectListItem {Value = "14,90", Text = "14.90 %"},
                       }, "Value", "Text");

            Active = true;
        }

    }
}